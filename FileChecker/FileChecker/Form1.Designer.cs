﻿namespace FileChecker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Directories = new System.Windows.Forms.TreeView();
            this.progressLabel = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.targetText = new System.Windows.Forms.TextBox();
            this.logBox = new System.Windows.Forms.RichTextBox();
            this.startDirText = new System.Windows.Forms.TextBox();
            this.filePatternText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.allFileCount = new System.Windows.Forms.Label();
            this.suitableFilesCount = new System.Windows.Forms.Label();
            this.aboutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Directories
            // 
            this.Directories.Location = new System.Drawing.Point(16, 12);
            this.Directories.Name = "Directories";
            this.Directories.Size = new System.Drawing.Size(338, 314);
            this.Directories.TabIndex = 0;
            this.Directories.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView1_AfterSelect);
            // 
            // progressLabel
            // 
            this.progressLabel.AutoSize = true;
            this.progressLabel.Location = new System.Drawing.Point(13, 332);
            this.progressLabel.Name = "progressLabel";
            this.progressLabel.Size = new System.Drawing.Size(48, 13);
            this.progressLabel.TabIndex = 1;
            this.progressLabel.Text = "Progress";
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(886, 15);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 2;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.Location = new System.Drawing.Point(886, 43);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(75, 23);
            this.pauseButton.TabIndex = 3;
            this.pauseButton.Text = "Pause";
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // targetText
            // 
            this.targetText.Location = new System.Drawing.Point(498, 69);
            this.targetText.Name = "targetText";
            this.targetText.Size = new System.Drawing.Size(382, 20);
            this.targetText.TabIndex = 4;
            this.targetText.TextChanged += new System.EventHandler(this.targetText_TextChanged);
            // 
            // logBox
            // 
            this.logBox.Location = new System.Drawing.Point(360, 249);
            this.logBox.Name = "logBox";
            this.logBox.ReadOnly = true;
            this.logBox.Size = new System.Drawing.Size(517, 77);
            this.logBox.TabIndex = 5;
            this.logBox.Text = "Logs:";
            // 
            // startDirText
            // 
            this.startDirText.Location = new System.Drawing.Point(498, 43);
            this.startDirText.Name = "startDirText";
            this.startDirText.Size = new System.Drawing.Size(382, 20);
            this.startDirText.TabIndex = 6;
            this.startDirText.TextChanged += new System.EventHandler(this.startDirText_TextChanged);
            // 
            // filePatternText
            // 
            this.filePatternText.Location = new System.Drawing.Point(498, 17);
            this.filePatternText.Name = "filePatternText";
            this.filePatternText.Size = new System.Drawing.Size(382, 20);
            this.filePatternText.TabIndex = 7;
            this.filePatternText.TextChanged += new System.EventHandler(this.filePatternText_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(418, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Start Directory";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(420, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Name Pattern";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(430, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Target Text";
            // 
            // allFileCount
            // 
            this.allFileCount.AutoSize = true;
            this.allFileCount.Location = new System.Drawing.Point(357, 220);
            this.allFileCount.Name = "allFileCount";
            this.allFileCount.Size = new System.Drawing.Size(80, 13);
            this.allFileCount.TabIndex = 11;
            this.allFileCount.Text = "Files Checked: ";
            // 
            // suitableFilesCount
            // 
            this.suitableFilesCount.AutoSize = true;
            this.suitableFilesCount.Location = new System.Drawing.Point(357, 233);
            this.suitableFilesCount.Name = "suitableFilesCount";
            this.suitableFilesCount.Size = new System.Drawing.Size(104, 13);
            this.suitableFilesCount.TabIndex = 12;
            this.suitableFilesCount.Text = "Fits The Description:";
            // 
            // aboutButton
            // 
            this.aboutButton.Location = new System.Drawing.Point(887, 73);
            this.aboutButton.Name = "aboutButton";
            this.aboutButton.Size = new System.Drawing.Size(75, 23);
            this.aboutButton.TabIndex = 13;
            this.aboutButton.Text = "About";
            this.aboutButton.UseVisualStyleBackColor = true;
            this.aboutButton.Click += new System.EventHandler(this.aboutButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 354);
            this.Controls.Add(this.aboutButton);
            this.Controls.Add(this.suitableFilesCount);
            this.Controls.Add(this.allFileCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filePatternText);
            this.Controls.Add(this.startDirText);
            this.Controls.Add(this.logBox);
            this.Controls.Add(this.targetText);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.progressLabel);
            this.Controls.Add(this.Directories);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(989, 393);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(989, 393);
            this.Name = "Form1";
            this.Text = "File Search";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView Directories;
        private System.Windows.Forms.Label progressLabel;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.TextBox targetText;
        private System.Windows.Forms.RichTextBox logBox;
        private System.Windows.Forms.TextBox startDirText;
        private System.Windows.Forms.TextBox filePatternText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label allFileCount;
        private System.Windows.Forms.Label suitableFilesCount;
        private System.Windows.Forms.Button aboutButton;
    }
}

