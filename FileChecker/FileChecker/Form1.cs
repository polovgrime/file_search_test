﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileChecker
{
    public enum SearchState
    {
        ALL, FOUND
    }
    public partial class Form1 : Form
    {
        Searcher searcher;
        ManualResetEvent me = new ManualResetEvent(false);
        bool pauseState = false;
        char[] invalidFileChars;
        char[] invalidDirectoryChars;
        const string NAME_PATTERN = "namePattern: ";
        const string START_DIR = "startDirectory: ";
        const string FILE_TEXT = "fileText: ";
        const string FILE_SAVE_NAME = "data.searchFile";
        const string ABOUT_TEXT = "Name pattern doesn't support regular expressions. But you can use " +
                               "\n\"*t.txt to get all .txt files ending with t, \"*.*\" to get files with" +
                               "\n all file extensions or \"*\" to get all files. " +
                               "\n To get files from all drives leave Start Directory box empty, to get " +
                               "\n files from drive C (D, E, etc) type C:\\ into Start Directory box, to " +
                               "\n get files from a specific directory type full directory into Start " +
                               "\n Directory box. Thank you";
        public Form1()
        {
            InitializeComponent();
            me.Set();
            // searcher = new Searcher(progressLabel, Directories, "", logBox, me);
            invalidDirectoryChars = Path.GetInvalidPathChars();
            invalidFileChars = Path.GetInvalidFileNameChars();
            List<char> invChars = invalidFileChars.ToList();
            invChars.Remove('*');
            invChars.Remove('?');
            invalidFileChars = invChars.ToArray();
            logBox.Text += "\n " + invalidDirectoryChars.Length + " " + invalidFileChars.Length;

        }
        int filesChecked = 0;
        int filesSuits = 0;
        void CountFile(int count, SearchState state)
        {
            switch (state)
            {
                case SearchState.ALL:
                    filesChecked += count;
                    allFileCount.Invoke(new Action(() => allFileCount.Text = "Files Checked: " + filesChecked));
                break;
                case SearchState.FOUND:
                    filesChecked += count;
                    allFileCount.Invoke(new Action(() => allFileCount.Text = "Files Checked: " + filesChecked));
                    filesSuits += count;
                    suitableFilesCount.Invoke(new Action(() => suitableFilesCount.Text = "Fits The Description:" + filesSuits));
                break;
            }
        }

        void OnSearchDone()
        {
            startButton.Invoke(new Action(() => { startButton.Enabled = true; }));
        }

        private void TreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            if (searcher != null)
            {
                searcher.Dispose();
                Directories.Nodes.Clear();
                pauseState = false;
                pauseButton.Text = "Pause";
                me.Set();
            }
            startButton.Enabled = false;
            logBox.Text = "Logs: ";
            filesChecked = 0;
            allFileCount.Text = "Files Checked: 0";
            suitableFilesCount.Text = "Fits The Description: 0";
            filesSuits = 0;
            if (filePatternText.Text == "")
                filePatternText.Text = "*";
            searcher = new Searcher(progressLabel, Directories, targetText.Text, filePatternText.Text, startDirText.Text, logBox, me, new Count (CountFile));
            searcher.SearchDone += OnSearchDone;

            searcher.Start();
        }

        private void targetText_TextChanged(object sender, EventArgs e)
        {

        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            if (!pauseState)
            {
                startButton.Enabled = true;
                pauseState = true;
                pauseButton.Text = "Resume";
                me.Reset(); 
            } else
            {
                pauseState = false;
                startButton.Enabled = false;
                pauseButton.Text = "Pause";
                me.Set();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(FILE_SAVE_NAME))
            {

                try
                {
                    string[] fileLines = File.ReadAllLines(FILE_SAVE_NAME);
                    foreach (var line in fileLines)
                    {
                        if (line.Contains(NAME_PATTERN))
                        {
                            filePatternText.Text = line.Replace(NAME_PATTERN, "");
                        } else if (line.Contains(START_DIR))
                        {
                            startDirText.Text = line.Replace(START_DIR, "");
                        } else if (line.Contains(FILE_TEXT))
                        {
                            targetText.Text = line.Replace(FILE_TEXT, "");
                        }
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void startDirText_TextChanged(object sender, EventArgs e)
        {
            if (startDirText.Text.Length <= 0)
                return;
            startDirText.Text = startDirText.Text.Trim(invalidDirectoryChars);
            
            
        }

        private void filePatternText_TextChanged(object sender, EventArgs e)
        {
            if (filePatternText.Text.Length <= 0)
                return;
            filePatternText.Text = filePatternText.Text.Trim(invalidFileChars);
            string testString = filePatternText.Text.Replace("**", "*");

            while (filePatternText.Text != testString)
            {
                testString = filePatternText.Text.Replace("**", "*");
                filePatternText.Text = testString;
            }

           
        }

        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (File.Exists(FILE_SAVE_NAME))
            {
                File.Delete(FILE_SAVE_NAME);
                
            }
            try
            {
                
                string[] lines = new string[3];
                lines[0] = NAME_PATTERN + filePatternText.Text;
                lines[1] = START_DIR + startDirText.Text;
                lines[2] = FILE_TEXT + targetText.Text;
                File.WriteAllLines(FILE_SAVE_NAME, lines);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(ABOUT_TEXT);
        }
    }
}
