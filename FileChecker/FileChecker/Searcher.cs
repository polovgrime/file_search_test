﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileChecker
{
    public delegate void Count(int count, SearchState state);
    public delegate void TriggerHandler();
    public class Searcher : IDisposable
    {
        public event TriggerHandler SearchDone;
        Label _progressLabel;
        RichTextBox _logBox;
        TreeView _resultTree;
        string _targetText;
        string _startDir;
        string _searchPattern;
        Count _Count;
        ManualResetEvent _manualResetEvent;
        bool abort = false;
        public Searcher(Label progress, TreeView result, string targetText, string searchPattern, string startDirectory, RichTextBox log, ManualResetEvent me,  Count Count)
        {
            _progressLabel = progress;
            _resultTree = result;
            _targetText = targetText;
            _logBox = log;
            _manualResetEvent = me;
            _searchPattern = searchPattern;
            _startDir = startDirectory;
            _Count = Count;
        }
        public void Start()
        {
            Thread t = new Thread(StartSearch);
            t.Start();

        }
        async void StartSearch()
        {
            bool test = await Search();
            if (test && !abort)
                SearchDone?.Invoke();
        }


        async Task<bool> Search()
        {
            return await Task.Run(() => {
                if (_startDir == "")
                {

                    string[] baseDirectory = Directory.GetLogicalDrives();
                    List<string> notUniqueDirectories;
                    List<string> childDirectories;
                    string parentDirectory;
                    foreach (var drive in baseDirectory)
                    {
                        _manualResetEvent.WaitOne();
                        if (abort)
                        {
                            return false;
                        }
                        notUniqueDirectories = new List<string>();
                        try
                        {
                            childDirectories = Directory.GetDirectories(drive).ToList();

                        }
                        catch (Exception e)
                        {
                            _progressLabel.Invoke(new Action(() => { _progressLabel.Text = e.Message; }));
                            continue;
                        }
                        parentDirectory = drive;
                        TreeNode node = new TreeNode(drive);
                        //   _resultTree.Invoke(new Action( () => { _resultTree.Nodes.Add(node); } ));
                        ScanDirectory(new DirectoryInfo(drive));
                    }
                    if (!abort)
                        _progressLabel.Invoke(new Action(() => { _progressLabel.Text = "Дело сделано"; }));

                    return true;
                } else
                {
                    ScanDirectory(new DirectoryInfo(_startDir));
                    return true;
                }

            });
        }
        

        void AddToTree(string path)
        {
            List<DirectoryInfo> directoryInfos = new List<DirectoryInfo>();
            DirectoryInfo dir = new DirectoryInfo(path);
            directoryInfos.Add(dir);
            DirectoryInfo temp = dir;
            do
            {
                directoryInfos.Add(temp);
                if (temp.FullName != dir.Root.FullName)
                    temp = temp.Parent;
                else
                    break;
            } while (true);
            directoryInfos.Reverse();
            var tempnode = _resultTree.Nodes;
            foreach (var di in directoryInfos)
            {
                bool exists = false;
                TreeNode tnode = new TreeNode();
                foreach (TreeNode node in tempnode)
                {
                    if (node.Name == di.Name)
                    {
                        exists = true;
                        tnode = node;
                        break;
                    }
                }
                if (exists)
                    tempnode = tnode.Nodes;
                else
                    _resultTree.Invoke(new Action(() => { tempnode = tempnode.Add(di.Name, di.Name).Nodes; }));
            }
        }

        void ScanDirectory(DirectoryInfo currentDirectory)
        {
            string[] filesInDir = GetFiles(currentDirectory.FullName);

            if (abort)
                return;
            var childDirectories = new DirectoryInfo[0];
            try
            {
                childDirectories = currentDirectory.GetDirectories();

            } catch (Exception e)
            {
                _progressLabel.Invoke(new Action( () => { _progressLabel.Text = e.Message; }));
            }
            if (childDirectories.Length != 0)
            {
                foreach (var childDir in childDirectories)
                {
                    _manualResetEvent.WaitOne();
                    if (abort)
                    {
                        return;
                    }
                    if (childDir.Attributes.HasFlag(FileAttributes.System))
                        continue;
                    ScanDirectory(childDir);
                }
            } 
        }

        string[] GetFiles(string path)
        {
            List<string> matchedFiles = new List<string>();
            List<string> files = new List<string>();
            try
            {
                files = Directory.GetFiles(path, _searchPattern).ToList();
             

            } catch (Exception e)
            {
                if (!abort)
                    _logBox.Invoke(new Action(() => { _logBox.Text += '\n' + e.Message ; }));

            }
            foreach (var file in files)
            {
                _manualResetEvent.WaitOne();
                if (abort)
                {
                    return null;
                }
                _progressLabel.Invoke(new Action(() => { _progressLabel.Text = file; }));
                bool doesMatch = false;
                try
                {
                    foreach(string line in File.ReadAllLines(file))
                    {
                        _manualResetEvent.WaitOne();
                        if (line.Contains(_targetText))
                        {
                            if (!abort)
                            {
                                _logBox.Invoke(new Action(() => { _logBox.Text += "\n \"" + _targetText + "\" is found in " + file; }));
                                matchedFiles.Add(file);
                                _Count(1, SearchState.FOUND);
                                doesMatch = true;
                                AddToTree(file);
                                break;

                            }
                        }
                    }
                    if (!doesMatch)
                        _Count(1, SearchState.ALL);
                } catch (Exception e)
                {
                    _progressLabel.Invoke(new Action(() => { _progressLabel.Text = e.Message; }));
                }
            }
            return matchedFiles.ToArray();
        }
        public void Dispose()
        {
            abort = true;
        }
    }
}
